package person_service

import "go-mysql/models"

type Person struct {
	UserId   int
	Username string
	Sex      string
	Email    string
}

func (p *Person) GetAll() ([]models.Person, error) {
	return models.GetAllPerson()
}
