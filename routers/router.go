package routers

import (
	v1 "go-mysql/routers/v1"
	"net/http"
)

//import "net/http"

const PREFIX = "/api/v1"
//const PREFIX = ""

func  InitRouters() *http.ServeMux {
	h := http.NewServeMux()

	h.HandleFunc(PREFIX + "/person", v1.GetPerson)

	return h
}