package v1

import (
	"fmt"
	"go-mysql/pkg/common"
	"go-mysql/pkg/e"
	"go-mysql/service/person_service"
	"net/http"
)

func GetPerson(req http.ResponseWriter, res *http.Request)  {
	appR := common.Res{C:req}
	p := person_service.Person{}
	person, err := p.GetAll()
	fmt.Println("获取到的人员：", person)
	if err != nil {
		appR.Response(e.DATABASE_ERROR, nil)
	}
	appR.Response(e.SUCCESS, person)
}