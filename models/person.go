package models

import "fmt"

type Person struct {
	UserId   int    `json:"user_id" db:"user_id"`
	Username string `json:"username" db:"username"`
	Sex      string `json:"sex" db:"sex"`
	Email    string `json:"email" db:"email"`
}

func GetAllPerson() ([]Person, error) {
	var person []Person

	err := db.Select(&person, "select user_id, username, sex, email from person")
	fmt.Println("models获取到的人员：", person)
	if err != nil {
		fmt.Println("exec failed, ", err)
		return nil, err
	}

	return person, nil
}