module go-mysql

go 1.14

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/jmoiron/sqlx v1.2.0
	gopkg.in/ini.v1 v1.60.0
)
