package main

import (
	"go-mysql/routers"
	_ "go-mysql/routers"
	"net/http"
)
func main() {
	println("学习mysql")
	routersInit := routers.InitRouters()
	server := &http.Server{
		Addr: "0.0.0.0:7246",
		Handler: routersInit,
	}
	_ = server.ListenAndServe()
}
