package common

import (
	"encoding/json"
	"go-mysql/pkg/e"
	"net/http"
)

type Res struct {
	C http.ResponseWriter
}

type  Response struct {
	Code int         `json:"code"`
	Data interface{} `json:"data"`
	Ext  interface{} `json:"ext"`
	Msg  string      `json:"msg"`
}

func (r *Res) Response(code int, data interface{}, ext ...interface{})  {
	res, _ := json.Marshal(Response{
		Code: code,
		Data: data,
		Ext: ext,
		Msg: e.GetMsg(code),
	})
	_, _ = r.C.Write(res)
	return
}